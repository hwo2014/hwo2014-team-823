"""A bootstrap file for running a bot"""
import socket
import sys
import logging


def _get_bot_class(bot_class_path):
    try:
        module_name, class_name = bot_class_path.rsplit(".", 1)
    except ValueError:
        raise ValueError("Invalid bot_class_path: '{}' "
                        "(it has to be in form 'module.Class')".format(bot_class_path))
    module = __import__(module_name, fromlist=[''])
    try:
        cls = getattr(module, class_name)
    except AttributeError:
        raise ImportError("Can't import name '{}' from '{}'".format(class_name, module_name))
    return cls


def main():
    LOG_FORMAT = "%(levelname)-8s %(name)-20s %(msg)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [bot_module.BotClass] [debug]")
    else:
        host, port, name, key = sys.argv[1:5]
        bot_class_path = sys.argv[5] if len(sys.argv) >= 6 else "bots.base.BaseBot"
        debug = sys.argv[6] if len(sys.argv) >= 7 else False
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        print("bot_class_path={0}, debug={1}".format(bot_class_path, debug))
        Bot = _get_bot_class(bot_class_path)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key, debug)
        bot.run()


if __name__ == "__main__":
    main()
