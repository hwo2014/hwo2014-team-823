import logging



class PID(object):

    __d_old = 0
    
    __i_old = 0
    
    __error = 0
    
    
    def __init__(self, kp, kd, ki, setpoint):
        self.kp = kp
        self.kd = kd
        self.ki = ki
        self.setpoint = setpoint
        self.logger = logging.getLogger("PID")


    def error(self, measured):
        __error = (measured - setpoint)
        return __error
    
    def derivate(self):
        # this is simplified derivation for embedded devices
        # should be: (error - old_error) / time_step
        #
        # but we assume timestep is one, to avoid division 
        
        dt = (__error - __d_old)

        __d_old = __error;
        return dt;
    
    def integrate(self):
        # simplified integration
        # assuming timestep is one
        
        i = __error + __i_old
        return i
        
        
    def step(self, value):
        error = error(value)
        i_value = integrate()
        d_value = derivate()
        
        output = ((self.kp * error) + (self.ki * i_value) + (self.kd * d_value))
        
        return output
        
        
        