import time
import socket
import logging
from multiprocessing import Process
from main import _get_bot_class

def test_race(track_name="keimola", car_count=2, *bot_paths):
    LOG_FORMAT = "%(levelname)-8s %(name)-20s %(msg)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    logger = logging.getLogger(__name__)
    host = "hakkinen.helloworldopen.com"
    port = 8091
    key = "I7JdH5leoY9FPw"
    default_bot_path = "bots.base.InteractiveBot"
    password = "whapadupidamdumbum"

    logger.info("Starting race. track: {}, car_count: {}, bot_paths: {}".format(
            track_name, car_count, bot_paths))

    def launch_bot(name, bot_path, debug):
        logger.info("Launching bot name: {}, path: {}, debug: {}".format(
                name, bot_path, debug))
        Bot = _get_bot_class(bot_path)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key, debug)
        bot.run(track_name=track_name, car_count=car_count, password=password)

    processes = []
    for i in range(car_count):
        path = both_paths[i] if len(bot_paths) > i else default_bot_path
        debug = True
        #debug = i == 0
        process = Process(target=launch_bot, args=("bot%s" % i, path, debug))
        process.start()
        processes.append(process)
        time.sleep(1)

    for process in processes:
        process.join()


if __name__ == "__main__":
    import sys
    test_race(*sys.argv[1:])
