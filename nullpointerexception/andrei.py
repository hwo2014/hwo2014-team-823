import logging
import random
import json
import math
from pprint import pprint
import pygame
import pygame.locals as pg
from multiprocessing import Queue, Process
from Queue import Empty as QueueEmpty


logger = logging.getLogger(__name__)

# testserver.helloworldopen.com 8091 nullpointerexception I7JdH5leoY9FPw

class Ui(object):
    @classmethod
    def launch_as_process(cls, queue=None):
        if queue is None:
            queue = Queue()
        ui_process = Process(target=Ui.launch, args=(queue,))
        ui_process.start()
        return ui_process, queue

    @classmethod
    def launch(cls, queue):
        ui = cls(queue)
        ui.run()

    def __init__(self, message_queue):
        logger.info("Starting ui")
        pygame.init()
        self.screen = pygame.display.set_mode((800, 600))
        self.track = Track()
        self.needs_redraw = True
        self.messages = message_queue
        self.logger = logging.getLogger("ui")

    def on_game_init(self, data):
        self.track.init(data)

    def on_car_positions(self, data):
        self.track.update_cars(data)

    def process_message(self, message):
        msg_map = {
            "gameInit": self.on_game_init,
            "carPositions": self.on_car_positions,
            # fullCarPositions is for test data ripped from race visualizer
            "fullCarPositions": self.on_car_positions,
        }
        msg_type, data = message["msgType"], message["data"]
        func = msg_map.get(msg_type, None)
        if func:
            #self.logger.info("Got message {}".format(message["msgType"]))
            func(data)
            self.needs_redraw = True
        else:
            #self.logger.info("Got unhandled message {}".format(message["msgType"]))
            pass

    def run(self):
        clock = pygame.time.Clock()
        while True:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pg.QUIT:
                    return
                elif event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
                    return
            try:
                message = self.messages.get_nowait()
                self.process_message(message)
            except QueueEmpty:
                pass
            if self.needs_redraw:
                #self.logger.info("REDRAW")
                self.screen.fill((50, 80, 110))
                self.track.draw(self.screen)
                pygame.display.update()
                self.needs_redraw = False


class Car(object):
    def __init__(self, track, name, color_name, length, width, guide_flag_pos):
        self.track = track
        self.name = name
        self.color_name = color_name
        self.length = length
        self.width = width
        self.guide_flag_pos = guide_flag_pos
        self.update(90.0, 0, 0.0, 0, 0, 0)

    def update(self, angle, piece_index, piece_distance, start_lane_index,
               end_lane_index, lap):
        self.angle = angle
        self.piece_index = piece_index
        self.piece_distance = piece_distance
        self.start_lane_index = start_lane_index
        self.end_lane_index = end_lane_index
        self.lap = lap

    def draw(self, surface, displacement=(0,0), scale=1):
        color_map = {
            "red": (255, 0, 0),
            "blue": (0, 0, 255),
            "green": (0, 255, 0),
            "_default": (255, 255, 255),
        }

        x, y = self.track.pieces[self.piece_index].get_position(self.piece_distance)
        img = pygame.Surface((self.length, self.width))
        img.fill(color_map.get(self.color_name, color_map["_default"]))
        img = img.convert_alpha()
        img = pygame.transform.scale(img, map(int, (self.length / scale, self.width / scale)))
        img = pygame.transform.rotate(img, -(self.angle - 90.0))
        x, y = transform_position((x, y), displacement, scale)
        rect = img.get_rect()
        rect.center = (x, y)
        surface.blit(img, rect)


class Track(object):
    def __init__(self):
        self.pieces = []
        self.cars = {}

    def init(self, data):
        start = data["race"]["track"]["startingPoint"]
        start_position = start["position"]["x"], start["position"]["y"]
        start_angle = start["angle"] - 90
        for piece_data in data["race"]["track"]["pieces"]:
            if piece_data.has_key("length"):
                piece = Straight(
                    start=start_position,
                    start_angle=start_angle,
                    length=piece_data["length"],
                    switch=piece_data.get("switch", False),
                )
            else:
                piece = Bend(
                    start=start_position,
                    start_angle=start_angle,
                    angle=piece_data["angle"],
                    radius=piece_data["radius"],
                    switch=piece_data.get("switch", False),
                )
            start_position = piece.end
            start_angle += piece_data.get("angle", 0)
            self.pieces.append(piece)
        for car_data in data["race"]["cars"]:
            car = Car(
                track=self,
                name=car_data["id"]["name"],
                color_name=car_data["id"]["color"],
                length=car_data["dimensions"]["length"],
                width=car_data["dimensions"]["width"],
                guide_flag_pos=car_data["dimensions"]["guideFlagPosition"],
            )
            self.cars[car_data["id"]["color"]] = car

    def update_cars(self, data):
        for car_data in data:
            color = car_data["id"]["color"]
            self.cars[color].update(
                angle=car_data["angle"],
                piece_index=car_data["piecePosition"]["pieceIndex"],
                piece_distance=car_data["piecePosition"]["inPieceDistance"],
                start_lane_index=car_data["piecePosition"]["lane"]["startLaneIndex"],
                end_lane_index=car_data["piecePosition"]["lane"]["endLaneIndex"],
                lap=car_data["piecePosition"]["lap"]
            )

    def draw(self, surface):
        displacement = 1200, 400
        scale = 2
        for i, piece in enumerate(self.pieces):
            color = map(lambda i: i % 255, [50 + i * 30, i * 30, i * 30])
            piece.draw(surface, color=color, displacement=displacement, scale=scale)
        for car in self.cars.values():
            car.draw(surface, displacement=displacement, scale=scale)


def transform_position(position, displacement, scale):
    position = (displacement[0] + position[0],
                displacement[1] + position[1])
    position = map(lambda val: val / scale, position)
    return position


def angle_distance(angle, distance, start=(0, 0)):
    rads = math.radians(angle)
    dx = distance * math.cos(rads)
    dy = distance * math.sin(rads)
    return start[0] + dx, start[1] + dy


class Straight(object):
    def __init__(self, start, start_angle, length, switch):
        self.switch = switch
        self.length = length
        self.start = start
        self.start_angle = start_angle
        angle_rads = math.radians(start_angle)
        dx = length * math.cos(angle_rads)
        dy = length * math.sin(angle_rads)
        self.end = start[0] + dx, start[1] + dy

    def draw(self, surface, color=(0,0,0), displacement=(0,0), scale=1):
        start = transform_position(self.start, displacement, scale)
        end = transform_position(self.end, displacement, scale)
        pygame.draw.line(surface, color, start, end, 4)

    def get_position(self, distance=0):
        angle_rads = math.radians(self.start_angle)
        dx = distance * math.cos(angle_rads)
        dy = distance * math.sin(angle_rads)
        return self.start[0] + dx, self.start[1] + dy


class Bend(Straight):  # TODO
    def __init__(self, start, start_angle, angle, radius, switch):
        self.switch = switch
        self.angle = angle
        self.radius = radius
        self.start = start
        self.start_angle = start_angle

        clockwise = angle >= 0
        center = angle_distance(start_angle + (90 if clockwise else -90),
                                radius, start)
        end = angle_distance(start_angle + (-90 if clockwise else 90) + angle,
                             radius, center)
        self.end = end
        self.start_angle = start_angle
        self.stop_angle = start_angle + angle
        self.arc_length = 2 * radius * math.pi * (abs(angle) / 360.0)
        self.line_length = math.sqrt(math.pow(end[0] - start[0], 2)
                                     + math.pow(end[1] - start[1], 2))

    def get_position(self, distance=0):
        percentage = (distance / self.arc_length)
        distance = percentage * self.line_length
        dx = self.end[0] - self.start[0]
        dy = self.end[1] - self.start[1]
        angle = math.atan2(dy, dx)
        return angle_distance(math.degrees(angle), distance, self.start)


def test_producer(queue):
    logger = logging.getLogger("producer")
    with open("test_data/race.json") as f:
        data = json.loads(f.read())
    logger.info("Starting test_producer")
    clock = pygame.time.Clock()
    for message in data:
        clock.tick(60)
        logger.info("Sending message {}".format(message["msgType"]))
        queue.put(message)


def main():
    LOG_FORMAT = "%(levelname)-8s %(name)-20s %(msg)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    queue = Queue()
    test_producer_process = Process(target=test_producer, args=(queue,))
    ui_process = Process(target=Ui.launch, args=(queue,))
    test_producer_process.start()
    ui_process.start()
    ui_process.join()
    test_producer_process.terminate()


if __name__ == "__main__":
    main()
