import logging
import json
from datetime import datetime


class BaseBot(object):
    """A base bot that implements a simple message loop architecture and debug
    hooks for running the bot with an ui and logging"""

    ## Initialization / cleanup

    def __init__(self, socket, name, key, debug=False):
        self.socket = socket
        self.name = name
        self.key = key
        self.logger = logging.getLogger(self.__class__.__name__)
        self.debug = debug
        self.color = ""
        self._current_tick = 0
        self.init()

    def init(self):
        """Callend from __init__. Extend this so __init__ won't have to be
        overwritten"""
        pass

    def set_up(self):
        """Ran at the start of bot.run(). Extend as you see fit (don't forget
        the super call)"""
        if self.debug:
            import os
            from visualizer import Ui
            filename = "%s.log" % datetime.now().strftime("%Y-%m-%d-%H%M%S")
            log_dir = "logs"
            filepath = os.path.join(log_dir, filename)
            if not os.path.exists(log_dir):
                os.makedirs(log_dir)
            self.log_file = open(filepath, "w")
            self.ui_process, self.ui_connection = Ui.launch_as_process()

    def tear_down(self):
        """Ran at the end of bot.run(). Quaranteed to be ran, even if the
        process raises an exception. Extend as you see fit (don't forget the
        super call)"""
        if getattr(self, "log_file", None):
            self.log_file.close()
        if getattr(self, "ui_process", None):
            self.ui_process.terminate()

    ## Running the bot

    def run(self, **join_args):
        self.set_up()
        try:
            if join_args:
                self.join_race(**join_args)
            else:
                self.join()
            self.msg_loop()
        finally:
            self.tear_down()

    def msg_loop(self):
        msg_map = self.get_msg_map()
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg.get("gameTick", None):
                self._current_tick = msg["gameTick"]
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                self.logger.info("Got {0}".format(msg_type))
                if msg.get("gameTick", None):
                    self.ping()
            # Debug stuff
            if getattr(self, "log_file", None):
                self.log_file.write(line)
            if getattr(self, "ui_connection", None):
                self.ui_connection.send(msg)
                if self.ui_connection.poll():
                    self.on_ui_event(self.ui_connection.recv())
            # Get new line
            line = socket_file.readline()

    ## Sending data to server

    def msg(self, msg_type, data):
        msg = {"msgType": msg_type, "data": data}
        if self._current_tick:
            msg["gameTick"] = self._current_tick
        self.send(json.dumps(msg))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, car_count=1, track_name=None, password=None):
        data = {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "carCount": car_count,
        }
        if track_name:
            data["trackName"] = track_name
        if password:
            data["password"] = password
        return self.msg("joinRace", data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def turbo(self):
        self.msg("turbo", "Hanat auki!")

    ## Reacting to data from server
    ## Dummy functions, extend as you see fit

    def get_msg_map(self):
        return {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            "yourCar": self.on_your_car,
            'error': self.on_error,
        }

    def on_join(self, data):
        self.logger.info("Joined")
        self.ping()

    def on_your_car(self, data):
        color = data["color"]
        self.logger.info("Got yourCar, color: {}".format(color))
        self.color = color

    def on_game_init(self, data):
        self.logger.info("Game initialized")
        self.ping()

    def on_game_start(self, data):
        self.logger.info("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.5)

    def on_crash(self, data):
        self.logger.info("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        self.logger.info("Race ended")
        self.ping()

    def on_error(self, data):
        self.logger.info("Error: {0}".format(data))
        self.ping()

    ## Interacting with the UI process

    def on_ui_event(self, event):
        """Process an event that comes from the UI. Implement as you see fit"""
        pass


class InteractiveBot(BaseBot):
    def init(self):
        self.throttle_speed = 0.5
        self.lane_switch = None
        self.use_turbo = False

    def on_car_positions(self, data):
        if self.use_turbo:
            self.turbo()
            self.use_turbo = False
        elif self.lane_switch:
            self.switch_lane(self.lane_switch)
            self.lane_switch = None
        else:
            self.throttle(self.throttle_speed)
        if getattr(self, "ui_connection", None):
            text = "Throttle: {}".format(self.throttle_speed)
            text += "\nColor: {}".format(self.color)
            self.ui_connection.send({
                "msgType": "setText",
                "data": text
            })

    def on_ui_event(self, event):
        UP = 273
        DOWN = 274
        RIGHT = 275
        LEFT = 276
        K_0 = 48
        K_9 = 57
        K_SPACE = 32

        if event["type"] == "key":
            if event["key"] == UP:
                self.throttle_speed += 0.1
            elif event["key"] == DOWN:
                self.throttle_speed -= 0.1
            elif event["key"] == LEFT:
                self.lane_switch = "Left"
            elif event["key"] == RIGHT:
                self.lane_switch = "Right"
            elif event["key"] == K_SPACE:
                self.use_turbo = True
            elif K_0 <= event["key"] <= K_9:
                # Beautiful
                self.throttle_speed = (event["key"] - K_0) / 10.0
