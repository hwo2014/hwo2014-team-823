from copy import copy
import math
from collections import namedtuple
import constants
import itertools
from .base import BaseBot


class Piece(object):
    @classmethod
    def list_from_data(cls, data):
        ret = []
        for piece in data:
            type = "straight" if piece.has_key("length") else "bend"
            ret.append(cls(
                type=type,
                switch=piece.get("switch", False),
                length=piece.get("length", None),
                radius=piece.get("radius", None),
                angle=piece.get("angle", None),
            ))
        return ret

    def __init__(self, type, switch=False, length=None, radius=None, angle=None):
        self.type = type
        self.switch = switch
        if type == "straight":
            self.length = length
        elif type == "bend":
            # TODO: lane length should be taken into account here
            self.length = 2 * radius * math.pi * (abs(angle) / 360.0)
            self.angle = angle
            self.radius = radius

    def get_lane_length(self, lane):
        # TODO: this should be different for type == "bend"
        return self.length

    def get_remaining_distance(self, lane, traveled):
        # TODO: this should be different for type == "bend"
        return self.get_lane_length(lane) - traveled


class State(object):
    def __init__(self, player):
        self.player = player

    def get_player_piece(self, world):
        return world.pieces[self.player.piece_index]

    def get_actions(self, world):
        piece = self.get_player_piece(world)
        throttles = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        lane_switches = [None]
        if piece.switch and self.player.start_lane == self.player.end_lane:
            if self.player.start_lane > 0:
                lane_switches.append("Left")
            if self.player.start_lane < (world.num_lanes - 1):
                lane_switches.append("Right")
        return list(itertools.product(throttles, lane_switches))

    def get_child(self, world, (throttle, lane_switch)):
        attrs = player.__dict__
        piece = world.pieces[self.player.piece_index]
        next_piece = world.pieces[(self.player.piece_index + 1) % len(world.pieces)]

        if lane_switch.lower() == "left":
            attrs["end_lane"] = attrs["start_lane"] - 1
        elif lane_switch.lower() == "right":
            attrs["end_lane"] = attrs["start_lane"] + 1

        # TODO

        return FrozenCar(**attrs)


def search(world, state):
    player = state.player
    current_piece = state.get_player_piece(world)

    # Determine next bend
    if current_piece.type == "bend":
        distance_until_bend = 0
        next_bend = current_piece
    else:
        distance_until_bend = current_piece.get_remaining_distance(
                player.start_lane, player.piece_distance)
        for piece in world.iterpieces(player.piece_index + 1):
            if piece.type == "bend":
                next_bend = piece
                break
            distance_until_bend += piece.get_lane_length(player.start_lane)

    # Determine next switch
    distance_until_switch = current_piece.get_remaining_distance(
            player.start_lane, player.piece_distance)
    for piece in world.iterpieces(player.piece_index + 1):
        if piece.switch:
            break
        distance_until_switch += piece.get_lane_length(player.start_lane)

    # Determine lane switch
    lane_switch = None
    can_switch = not player.is_switching and distance_until_switch < 100
    on_leftmost_lane = player.start_lane == 0
    on_rightmost_lane = player.start_lane == (world.num_lanes + 1)
    if can_switch:
        #print "CAN SWITCH",
        if next_bend.angle >= 0 and not on_rightmost_lane:
            #print "RIGHT"
            lane_switch = "Right"
        elif next_bend.angle < 0 and not on_leftmost_lane:
            #print "LEFT"
            lane_switch = "Left"
        else:
            #print "NONE"
            pass

    # Determine throttle
    throttle = 1.0
    react_distance = 200.0
    max_angle_speed = 6.7
    if distance_until_bend < react_distance:
        target_speed = ((distance_until_bend / react_distance) * 10.0
                        + (1.0 - (distance_until_bend / react_distance))
                           * max_angle_speed)
        if player.speed >= target_speed:
            throttle = 0.0
        else:
            throttle = target_speed / 10.0

    return throttle, lane_switch, state


class Struct(dict):
    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __getattr__(self, attr):
        return self.__dict__[attr]

    def __setattr__(self, attr, value):
        self.__dict__[attr] = value


class World(object):
    def __init__(self):
        self.pieces = []
        self.num_lanes = 1
        self.variables = Struct(
            friction=0.5,
        )

    #def get_acceleration(self, speed, throttle):
    #    pass

    def calibrate(self, current_state, predicted_state):
        pass

    def iterpieces(self, start=0):
        """Return an iterator that yields track pieces infinitely"""
        num_pieces = len(self.pieces)
        while True:
            yield self.pieces[start % num_pieces]
            start = (start + 1) % num_pieces


def evaluate_state(state):
    return (
        not state.player.crashed,
        state.player.lap,
        state.player.piece_index,
        state.player.piece_distance,
    )


class Car(object):
    hashable_keys = (
        "angle",
        "piece_index",
        "piece_distance",
        "lap",
        "start_lane",
        "end_lane",
        "speed",
        "acceleration",
        "crashed",
    )

    def __init__(self):
        self.lap = 0
        self.piece_index = 0
        self.piece_distance = 0
        self.angle = 0.0
        self.start_lane = 0
        self.end_lane = 0
        self.speed = 0.0
        self.acceleration = 0.0
        self.crashed = False
        for key in self.hashable_keys:
            assert hasattr(self, key)

    def __hash__(self):
        return hash(tuple(
            (k, v) for (k, v) in self.__dict__.items()
            if k in self.hashable_keys
        ))

    def get_hashable_data(self):
        return dict([
            (k, v) for (k, v) in self.__dict__.items()
            if k in self.hashable_keys
        ])

    def freeze(self):
        return FrozenCar(**self.get_hashable_data())

    def update(self, world, angle, lap, piece_index, piece_distance,
               start_lane, end_lane, ticks=1):
        # Calculate speed, acceleration, and whether the car has crashed
        if piece_index > self.piece_index:
            piece = world.pieces[self.piece_index]
            distance = (piece.get_lane_length(start_lane)
                        - self.piece_distance)
            distance += piece_distance
        else:
            distance = piece_distance - self.piece_distance
        last_speed = self.speed
        self.speed = distance / float(ticks)
        self.acceleration = self.speed - last_speed
        self.crashed = abs(self.angle) >= constants.CRASH_ANGLE_DEGREES
        # Set other data
        self.angle = angle
        self.piece_index = piece_index
        self.piece_distance = piece_distance
        self.lap = lap
        self.start_lane = start_lane
        self.end_lane = end_lane

    def update_from_data(self, world, data, ticks=1):
        """Update from a dict in the carPositions.data list"""
        self.update(
            world=world,
            angle=data["angle"],
            lap=data["piecePosition"]["lap"],
            piece_index=data["piecePosition"]["pieceIndex"],
            piece_distance=data["piecePosition"]["inPieceDistance"],
            start_lane=data["piecePosition"]["lane"]["startLaneIndex"],
            end_lane=data["piecePosition"]["lane"]["endLaneIndex"],
            ticks=ticks,
        )

    @property
    def is_switching(self):
        return self.start_lane != self.end_lane


FrozenCar = namedtuple("FrozenCar", Car.hashable_keys)
FrozenCar.is_switching = property(lambda s: s.start_lane != s.end_lane)


class SearchBot(BaseBot):
    def init(self):
        self.throttle_speed = 0.5
        self.world = World()
        self.car = Car()
        self.switch_in_progress = False

    def on_game_init(self, data):
        self.logger.info("Game initialized")
        self.world.pieces = Piece.list_from_data(data["race"]["track"]["pieces"])
        self.world.num_lanes = len(data["race"]["track"]["lanes"])
        self.ping()

    def on_crash(self, data):
        if data["name"] == self.name:
            self.logger.info("Crashed! Speed = {0:7.2f}".format(self.car.speed))
        self.ping()

    def on_car_positions(self, data):
        try:
            data = [d for d in data if d["id"]["name"] == self.name][0]
        except IndexError:
            self.logger.error("Can't get carPositions for this car")
            self.ping()
            return
        self.car.update_from_data(self.world, data, ticks=1)
        self.update_ui()
        # Krhm switch
        if self.car.is_switching:
            self.switch_in_progress = False
        # Lookahead
        state = State(player=self.car.freeze())
        throttle, lane_direction, predicted_state = search(self.world, state)
        # Act
        self.throttle_speed = throttle
        if lane_direction:
            self.switch_lane(lane_direction)
            self.switch_in_progress = True
        else:
            self.throttle(self.throttle_speed)

    def switch_lane(self, direction):
        super(SearchBot, self).switch_lane(direction)
        self.logger.info("Switch lanes to {}".format(direction))

    def on_ui_event(self, event):
        UP = 273
        DOWN = 274
        RIGHT = 275
        LEFT = 276
        if event["type"] == "key":
            if event["key"] == UP:
                self.throttle_speed += 0.1
            elif event["key"] == DOWN:
                self.throttle_speed -= 0.1
            elif event["key"] == LEFT:
                self.switch_lane("Left")
            elif event["key"] == RIGHT:
                self.switch_lane("Right")

    def update_ui(self):
        if getattr(self, "ui_connection", None):
            car = self.car
            text = ("Throttle: {0:7.2f}\n"
                    "Angle:    {1:7.2f}\n"
                    "Speed:    {2:7.2f} / tick\n"
                    "Accl:     {3:7.2f} / tick\n")
            text = text.format(self.throttle_speed, car.angle, car.speed,
                               car.acceleration)
            self.ui_connection.send({
                "msgType": "setText",
                "data": text
            })
