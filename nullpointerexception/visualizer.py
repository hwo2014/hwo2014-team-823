"""Race visualization to aid development. Not meant to be used in the actual
races (ie.  CI server)"""
import logging
import random
import json
import math
from pprint import pprint
import pygame
import pygame.locals as pg
from multiprocessing import Process, Pipe


logger = logging.getLogger(__name__)


class Ui(object):
    @classmethod
    def launch_as_process(cls, connection=None):
        if connection is None:
            connection, conn2 = Pipe()
        ui_process = Process(target=cls.launch, args=(connection,))
        ui_process.start()
        return ui_process, conn2

    @classmethod
    def launch(cls, connection=None):
        ui = cls(connection=connection)
        ui.run()

    def __init__(self, connection=None):
        logger.info("Starting ui")
        pygame.init()
        self.screen = pygame.display.set_mode((800, 600))
        self.track = Track()
        self.needs_redraw = True
        self.connection = connection
        self.logger = logging.getLogger("ui")
        self.tick = 0
        self.bot_text = ""

    def on_game_init(self, data):
        self.track.init(data)

    def on_car_positions(self, data):
        self.track.update_cars(data)

    def on_set_text(self, data):
        """A faux message that can be used to draw a text from the client to
        the ui"""
        self.bot_text = data
        self.needs_redraw = True

    def process_message(self, message):
        msg_map = {
            "gameInit": self.on_game_init,
            "carPositions": self.on_car_positions,
            # fullCarPositions is for test data ripped from race visualizer
            "fullCarPositions": self.on_car_positions,

            # pseudo-messages
            "setText": self.on_set_text,
        }
        msg_type, data = message["msgType"], message["data"]
        if message.get("gameTick", None):
            self.tick = message["gameTick"]
        func = msg_map.get(msg_type, None)
        if func:
            #self.logger.info("Got message {}".format(message["msgType"]))
            func(data)
            self.needs_redraw = True
        else:
            #self.logger.info("Got unhandled message {}".format(message["msgType"]))
            pass

    def run(self):
        clock = pygame.time.Clock()
        while True:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pg.QUIT:
                    return
                elif event.type == pg.KEYDOWN:
                    self.connection.send({"type": "key", "key": event.key})
                    if event.key == pg.K_ESCAPE:
                        return
            if self.connection and self.connection.poll():
                message = self.connection.recv()
                self.process_message(message)
            if self.needs_redraw:
                self.screen.fill((50, 80, 110))
                self.track.draw(self.screen)
                if self.bot_text:
                    text = draw_text(self.bot_text)
                    rect = text.get_rect()
                    rect.left = 20
                    rect.bottom = 550
                    self.screen.blit(text, rect)
                self.screen.blit(draw_text("Tick: %07s" % self.tick), (20, 550))
                pygame.display.update()
                self.needs_redraw = False


class Car(object):
    def __init__(self, track, name, color_name, length, width, guide_flag_pos):
        self.track = track
        self.name = name
        self.color_name = color_name
        self.length = length
        self.width = width
        self.guide_flag_pos = guide_flag_pos
        self.update(90.0, 0, 0.0, 0, 0, 0)

    def update(self, angle, piece_index, piece_distance, start_lane_index,
               end_lane_index, lap):
        self.angle = angle
        self.piece_index = piece_index
        self.piece_distance = piece_distance
        self.start_lane_index = start_lane_index
        self.end_lane_index = end_lane_index
        self.lap = lap

    def draw(self, surface, displacement=(0,0), scale=1):
        color_map = {
            "red": (255, 0, 0),
            "blue": (0, 0, 255),
            "green": (0, 255, 0),
            "_default": (255, 255, 255),
        }

        piece = self.track.pieces[self.piece_index]
        x, y = piece.get_position(self.piece_distance, self.start_lane_index)
        if self.start_lane_index != self.end_lane_index:
            # Switching lanes. slooow....
            percentage = self.piece_distance / float(piece.get_length())
            x2, y2 = piece.get_position(self.piece_distance, self.end_lane_index)
            x, y = (int(x * (1.0 - percentage) + x2 * percentage),
                    int(y * (1.0 - percentage) + y2 * percentage))

        angle = piece.get_car_angle() + self.angle
        img = pygame.Surface((self.length, self.width))
        img.fill(color_map.get(self.color_name, color_map["_default"]))
        img = img.convert_alpha()
        img = pygame.transform.scale(img, map(int, (self.length / scale, self.width / scale)))
        img = pygame.transform.rotate(img, -angle)
        x, y = transform_position((x, y), displacement, scale)
        rect = img.get_rect()
        rect.center = (x, y)
        surface.blit(img, rect)


class Track(object):
    def __init__(self):
        self.pieces = []
        self.cars = {}

    def init(self, data):
        start = data["race"]["track"]["startingPoint"]
        start_position = start["position"]["x"], start["position"]["y"]
        start_angle = start["angle"] - 90
        lanes = [l["distanceFromCenter"] for l in data["race"]["track"]["lanes"]]
        for piece_data in data["race"]["track"]["pieces"]:
            if piece_data.has_key("length"):
                piece = Straight(
                    start=start_position,
                    start_angle=start_angle,
                    length=piece_data["length"],
                    switch=piece_data.get("switch", False),
                    lanes=lanes,
                )
            else:
                piece = Bend(
                    start=start_position,
                    start_angle=start_angle,
                    angle=piece_data["angle"],
                    radius=piece_data["radius"],
                    switch=piece_data.get("switch", False),
                    lanes=lanes,
                )
            start_position = piece.end
            start_angle += piece_data.get("angle", 0)
            self.pieces.append(piece)
        for car_data in data["race"]["cars"]:
            car = Car(
                track=self,
                name=car_data["id"]["name"],
                color_name=car_data["id"]["color"],
                length=car_data["dimensions"]["length"],
                width=car_data["dimensions"]["width"],
                guide_flag_pos=car_data["dimensions"]["guideFlagPosition"],
            )
            self.cars[car_data["id"]["color"]] = car

    def update_cars(self, data):
        for car_data in data:
            color = car_data["id"]["color"]
            self.cars[color].update(
                angle=car_data["angle"],
                piece_index=car_data["piecePosition"]["pieceIndex"],
                piece_distance=car_data["piecePosition"]["inPieceDistance"],
                start_lane_index=car_data["piecePosition"]["lane"]["startLaneIndex"],
                end_lane_index=car_data["piecePosition"]["lane"]["endLaneIndex"],
                lap=car_data["piecePosition"]["lap"]
            )

    def draw(self, surface):
        displacement = 1200, 400
        scale = 2
        for i, piece in enumerate(self.pieces):
            color = map(lambda i: i % 255, [50 + i * 30, i * 30, i * 30])
            piece.draw(surface, color=color, displacement=displacement, scale=scale)
        for car in self.cars.values():
            car.draw(surface, displacement=displacement, scale=scale)


__font = None
def draw_text(text, color=(255, 255, 255)):
    global __font
    if not __font:
        __font = pygame.font.Font(None, 36)
    if "\n" not in text:  # speedup
        return __font.render(text, 1, color)

    # multiline text
    surfaces = []
    for line in text.split("\n"):
        surfaces.append(__font.render(line, 1, color))
    rects = [s.get_rect() for s in surfaces]
    width = max(r.w for r in rects)
    height = sum(r.h for r in rects)
    surface  = pygame.Surface((width, height)).convert_alpha()
    surface.fill((255, 255, 255, 0.0))
    previous_bottom = 0
    for surf, rect in zip(surfaces, rects):
        rect.top = previous_bottom
        surface.blit(surf, rect)
        previous_bottom = rect.bottom
    return surface


def transform_position(position, displacement, scale):
    position = (displacement[0] + position[0],
                displacement[1] + position[1])
    position = map(lambda val: val / scale, position)
    return position


def angle_distance(angle, distance, start=(0, 0)):
    rads = math.radians(angle)
    dx = distance * math.cos(rads)
    dy = distance * math.sin(rads)
    return start[0] + dx, start[1] + dy


def perpendicular_distance(angle, distance, pos):
    return angle_distance(angle + 90, distance, pos)


class Straight(object):
    def __init__(self, start, start_angle, length, switch, lanes):
        self.switch = switch
        self.length = length
        self.start = start
        self.start_angle = start_angle
        self.stop_angle = start_angle
        self.lanes = lanes
        angle_rads = math.radians(start_angle)
        dx = length * math.cos(angle_rads)
        dy = length * math.sin(angle_rads)
        self.end = start[0] + dx, start[1] + dy

    def get_car_angle(self):
        return self.stop_angle

    def draw(self, surface, color=(0,0,0), displacement=(0,0), scale=1):
        width = 2
        start = transform_position(self.start, displacement, scale)
        end = transform_position(self.end, displacement, scale)
        for lane_distance in self.lanes:
            s = perpendicular_distance(self.stop_angle, lane_distance,
                                       start)
            e = perpendicular_distance(self.stop_angle, lane_distance,
                                       end)
            self._draw_line(surface, color, s, e, width)
        if self.switch:
            halfway = (int(start[0] + ((end[0] - start[0]) / 2.0)),
                       int(start[1] + ((end[1] - start[1]) / 2.0)))
            s1 = perpendicular_distance(self.stop_angle, self.lanes[0],
                                        start)
            e1 = perpendicular_distance(self.stop_angle, self.lanes[-1],
                                        end)
            s2 = perpendicular_distance(self.stop_angle, self.lanes[-1],
                                        start)
            e2 = perpendicular_distance(self.stop_angle, self.lanes[0],
                                        end)
            pygame.draw.line(surface, color, s1, e1, width)
            pygame.draw.line(surface, color, s2, e2, width)

    def _draw_line(self, surface, color, start, end, width):
        pygame.draw.line(surface, color, start, end, width)

    def get_lane_distance(self, lane_index):
        return self.lanes[lane_index] if lane_index is not None \
                else self.lanes[len(self.lanes) / 2]

    def get_length(self):
        return self.length

    def get_position(self, distance=0, lane_index=None):
        pos = angle_distance(self.start_angle, distance, self.start)
        lane_distance = self.get_lane_distance(lane_index)
        return perpendicular_distance(self.start_angle, lane_distance, pos)


class Bend(Straight):  # TODO
    def __init__(self, start, start_angle, angle, radius, switch, lanes):
        self.switch = switch
        self.angle = angle
        self.radius = radius
        self.start = start
        self.start_angle = start_angle
        self.lanes = lanes

        clockwise = angle >= 0
        center = angle_distance(start_angle + (90 if clockwise else -90),
                                radius, start)
        end = angle_distance(start_angle + (-90 if clockwise else 90) + angle,
                             radius, center)
        self.end = end
        self.start_angle = start_angle
        self.arc_length = 2 * radius * math.pi * (abs(angle) / 360.0)
        self.line_length = math.sqrt(math.pow(end[0] - start[0], 2)
                                     + math.pow(end[1] - start[1], 2))

        _dx = self.end[0] - self.start[0]
        _dy = self.end[1] - self.start[1]
        self.stop_angle = math.degrees(math.atan2(_dy, _dx))

    def get_length(self):
        return self.arc_length

    def get_position(self, distance=0, lane_index=None):
        percentage = (distance / self.arc_length)
        distance = percentage * self.line_length
        pos = angle_distance(self.stop_angle, distance, self.start)
        lane_distance = self.get_lane_distance(lane_index)
        return perpendicular_distance(self.stop_angle, lane_distance, pos)


def test_producer(connection):
    logger = logging.getLogger("producer")
    with open("test_data/race.json") as f:
        data = json.loads(f.read())
    logger.info("Starting test_producer")
    clock = pygame.time.Clock()
    for message in data:
        clock.tick(60)
        logger.info("Sending message {}".format(message["msgType"]))
        connection.send(message)


def main():
    LOG_FORMAT = "%(levelname)-8s %(name)-20s %(msg)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    ui_process, connection = Ui.launch_as_process()
    test_producer_process = Process(target=test_producer, args=(connection,))
    test_producer_process.start()
    ui_process.join()
    test_producer_process.terminate()


if __name__ == "__main__":
    main()
